import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, IonicPage } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth.service';
import { MapPage } from '../map/map.component';
import { HomePage } from '../home/home.component';
import { ActivityTracker } from '../../providers/activity-tracker/activity-tracker.service';

@Component({
  selector: 'auth-page',
  templateUrl: 'auth.html',
})
export class AuthPage {
  private loading: Loading;
  private authState: any = null;
  private registerCredentials = { email: '', password: '' };
  private page: string = 'login';

  constructor(private nav: NavController, private auth: AuthService, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private activityTracker: ActivityTracker) { }

  private afterSignIn(): void {
    this.nav.setRoot(MapPage);
    this.activityTracker.scoreTimeout();
  }

  private afterSignOut(): void {
    this.nav.setRoot(HomePage);
  }

  signInWithGoogle(): void {
    this.auth.googleLogin()
      .then(() => this.afterSignIn());
  }

  signInWithFacebook(): void {
    this.auth.facebookLogin()
      .then(() => this.afterSignIn());
  }

  resetPassword(email: string) {
    this.auth.resetPassword(email);
  }

  signup(): void {
    this.auth.signup(this.registerCredentials.email, this.registerCredentials.password);
    this.email = this.password = '';
    this.afterSignIn();
  }

  login(): void {
    this.auth.login(this.registerCredentials.email, this.registerCredentials.password);
    this.email = this.password = '';
    this.afterSignIn();
  }

  logout(): void {
    this.auth.logout();
    this.afterSignOut();
  }

  public switch() {
    if (this.page === 'login') {
      this.page = 'register';
      this.registerCredentials = { email: '', password: '' };
      return;
    }
    this.page = 'login';
    this.registerCredentials = { email: '', password: '' };
  }
  //
  // showLoading() {
  //   this.loading = this.loadingCtrl.create({
  //     content: 'Please wait...',
  //     dismissOnPageChange: true
  //   });
  //   this.loading.present();
  // }

  showError(text) {
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}
