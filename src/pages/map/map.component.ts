import { identifierModuleUrl } from '@angular/compiler/compiler';
import { Component, NgZone, ViewChild, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActionSheetController, AlertController, App, LoadingController, NavController, Platform, ToastController, Events } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 GoogleMapOptions,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';

import { LocationTracker } from '../../providers/location-tracker/location-tracker.service';
import { ProbabilityTracker } from '../../providers/probability-tracker/probability-tracker.service';
import { HotspotInterface } from '../../interfaces';
import { FirebaseProvider } from "../../providers/firebase/firebase.service";


declare const google: any;

@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
  changeDetection: ChangeDetectionStrategy.Default
})

export class MapPage {
  @ViewChild('map') mapRef: ElementRef;
  //@ViewChild('fab') fab: FabContainer;

  constructor(private googleMaps: GoogleMaps,
              public navCtrl: NavController,
              public geo: Geolocation,
              public locationTracker: LocationTracker,
              private event: Events,
              private changeDetectorRef: ChangeDetectorRef,
              private probabilityTracker: ProbabilityTracker,
              private firebaseProvider: FirebaseProvider,
              private cdRef: ChangeDetectorRef
  ) {}

  public hotspots: Array<HotspotInterface> = [];
  public infoWindow = new google.maps.InfoWindow();
  public markers: Array<MarkerOptions> = [];
  public unauthorizedMarkers: Array<MarkerOptions> = [];
  public addHotspotListener: boolean = false;
  public infoWindowHover: boolean = false;
  public globalMap;
  public currentLocationMarker: MarkerOptions = null;
  public trackingListener: boolean = false;
  public probabilityRangeNumber: number = 0;
  public probabilityRangeListener: boolean = false;
  public closeProbabilityRangeListener: boolean = false;
  public isFabVisible: boolean = true;
  public Circle = new google.maps.Circle({
      strokeColor:   'black',
      strokeOpacity: 0.08,
      strokeWeight:  20,
      fillColor:     '#add8e6',
      fillOpacity:   0.40,
      radius:        2000,
      visible:       false,
      flag:          false
  });
  public modelRef: Observable<any[]>;
  public carIcon: MarkerOptions = null;
  public trackingLatLng: MarkerOptions;
  public firstTime: Object = {
    locationTracking: true,
    addHotspot: true,
    probabilityRange: true
  }
  private activate: HotspotInterface;
  //public dontShowMarkers: boolean = true;

  ionViewDidLoad() {
   this.globalMap = this.loadMap();
   if (this.locationTracker.shouldResumeTracking === true) {
     this.startTracking();
   }
   // subscribe to NOW parameter
   this.firebaseProvider.modelRef.subscribe((model) => {
     this.hotspots = model;
     this.updateMarkers();
   });

   //this.dontShowMarkers = this.firebaseProvider.getDontShowMarkers();
   //
   // this.firebaseProvider.dontShowMarkers.subscribe((res) => {
   //    this.dontShowMarkers = res
   // });
   //

   //this.firebaseProvider.addItem(data);
   //this.hotspots = this.probabilityTracker.getByHour('Monday', 5);

   // this.probabilityTracker.cast.subscribe((notify) => {
   //   this.hotspots.forEach((hotspot) => {
   //     if (hotspot.id === notify) {
   //       hotspot.now = true;
   //       this.refreshMarkers();
   //     }
   //   });
   // });

  };

  ionViewDidLeave() {
    if(this.trackingListener === true) {
        this.stopTracking();
    }
  }

  // getHotspotsByHour = (day, hour) => {
  //   this.deleteMarkers();
  //   this.hotspots = this.firebaseProvider.getByHour(day, hour);
  //   this.loadMap();
  // };
  //
  // getHotspotById = (id) => {
  //   this.hotspots.forEach((hotspot) => {
  //     if(hotspot.id === id) {
  //       let data: object = {
  //         day: hotspot.day,
  //         hour: hotspot.hour,
  //         id: hotspot.id
  //       };
  //       //this.hotspots[this.hotspots.indexOf(hotspot)] = this.firebaseProvider.getByHotspot(data);
  //       this.loadMap();
  //     }
  //   });
  // };

  updateHotspotQueue = (id, type) => {
    this.hotspots.forEach((hotspot) => {
      if(hotspot.id === id) {
        if(type === 'upvote') {
          hotspot.queue.upvote++;
        }
        else hotspot.queue.downvote++;
      }
      this.firebaseProvider.updateItem(hotspot);
    });
    this.updateMarkers();
  };

  updateHour = (day, hour) => {
    this.probabilityTracker.updateByHour(day, hour);
    //this.loadMap();
  };

  loadMap = () => {
    const mapOptions = {
        zoom:      12,
        center:    new google.maps.LatLng(44.32, 23.8),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        disableDefaultUI: true
    };
    const map: GoogleMap = new google.maps.Map(this.mapRef.nativeElement, mapOptions);
    this.Circle.setMap(map);

    google.maps.event.addListener(map, 'click', (e) => {
      //let probabilityOpen = false;
        if (this.probabilityRangeListener === true) {
            //probabilityOpen = true;
            this.probabilityRangeListener = false;
            this.isFabVisible = true;
            this.cdRef.detectChanges();
            return;
        }

        if(this.trackingListener === true) {
            return;
        }

        let myLatLng = new google.maps.LatLng(parseFloat(e.latLng.lat()), parseFloat(e.latLng.lng()));
        this.infoWindow.close();
        this.infoWindowHover = false;

        if (this.addHotspotListener) {
          // HERE AFTER TIME
          // let info = this.getTime();
          let info = {
            lat: 0,
            long: 0
          };
          info.lat = myLatLng.lat();
          info.long = myLatLng.lng();
        // DUPLICATE FOR TESTING
          this.createMarker(info, 'unauth', map);
          this.isFabVisible = true;
          this.cdRef.detectChanges();
        }

        if (this.Circle.flag === true) {
            this.Circle.flag = false;
            console.log("show visible");
            this.showVisible(this.Circle.getCenter());
            return;
        }
        this.showVisible(myLatLng);

    });

    this.loadMarkers(map);
    return map;
  };

  getTime = () => {
    //TODO

  };

  showVisible = (myLatLng) => {
    this.Circle.setCenter(myLatLng);
    this.Circle.setVisible(true);
    this.refreshMarkers();
  };

  addHotspot = (fab) => {
      if(this.addHotspotListener) {
        this.addHotspotListener = false;
        return;
      }
      if(this.trackingListener === true) {
        console.log("working");
          let info = {
            lat: this.trackingLatLng.lat(),
            long: this.trackingLatLng.lng()
          };
          this.createMarker(info, 'unauth', this.globalMap);
          return;
      }
      this.addHotspotListener = true;

      if (this.Circle.visible === true) {
          this.Circle.setVisible(false);
          this.Circle.flag = true;
      }
      this.markers.forEach( (marker) => {
          marker.setVisible(false);
      });
      fab.close();
  };

  disableInfoWindowVote = (info, type) => {
    info.voted = true;
    let voted;
    if (type === 'default')
      voted = '<div class="infoWindowDescription"><div class="infoWindowProbability">Probability: <span class="infoWindowProbabilityNo"><b>' + info.probability + '%</b></span><p class="infoWindowPretext">You may encounter difficulties if you choose to go this way.</p></div><div class="feedbackContainer centered">Thank you for your implication!</div></div>';
    else
      voted = '<div class="infoWindowDescription"><div class="infoWindowProbability">Police is here !<p class="infoWindowPretext" style="color:#d9534f"><b>RIGHT NOW</b></p></div><div class="feedbackContainer centered">Thank you for your implication!</div></div>';
    this.infoWindow.setContent(voted);
  };

  createMarker = (info, type, map) => {
      let myLatLng = new google.maps.LatLng(info.lat, info.long);
      let iconUrl = '../../assets/imgs/warning-icon-gray.png';
      if(info.now === true) {
          iconUrl = '../../assets/imgs/police-siren-icon.png';
      }
      else if (info.probability > 74) {
          iconUrl = '../../assets/imgs/warning-icon-red.png';
      }
      else if (info.probability > 49) {
          iconUrl = '../../assets/imgs/warning-icon-orange.png';
      }
      let icon   = {
          url:        iconUrl,
          scaledSize: new google.maps.Size(50, 50)
      };

      let marker = new google.maps.Marker({
          id: info.id,
          probability: info.probability,
          map:      map,
          animation: google.maps.Animation.DROP,
          position: myLatLng,
          icon:     icon,
          title:    '',
          visible:  false,
          visibleInTime: true,
          content:  '<div class="infoWindowDescription"><div class="infoWindowProbability">Probability: <span class="infoWindowProbabilityNo"><b>' + info.probability + '%</b></span><p class="infoWindowPretext">You may encounter difficulties if you choose to go this way.</p></div><div class="feedbackContainer centered"><div id="infoWindowRemove' + info.id + '" class="rosu"><span class="glyphicon glyphicon-remove"></span></div><div  id="infoWindowAccept' + info.id + '" class="verde"><span class="glyphicon glyphicon-ok"></span></div></div></div>'
      });
      if (type === 'default') {
          google.maps.event.addListener(marker, 'mouseover', () => {
              if(info.voted === true) {
                this.disableInfoWindowVote(info, 'default');
              }
              else {
                this.infoWindow.setContent(marker.content);
              }
              if(info.now === true) {
                this.infoWindow.setContent('<div class="infoWindowDescription"><div class="infoWindowProbability">Police is here !<p class="infoWindowPretext" style="color:#d9534f"><b>RIGHT NOW</b></p></div><div class="feedbackContainer centered"><div id="infoWindowRemove' + info.id + '" class="rosu"><span class="glyphicon glyphicon-remove"></span></div><div  id="infoWindowAccept' + info.id + '" class="verde"><span class="glyphicon glyphicon-ok"></span></div></div></div>');
                if (info.voted === true) {
                  this.disableInfoWindowVote(info, 'now');
                }
              }
              this.infoWindow.open(map, marker);
          });
          this.markers.push(marker);
          //console.log(this.markers);
      }
      else if (type === 'unauth') {
          this.addHotspotListener = false;
          this.cdRef.detectChanges();
          icon.url = '../../assets/imgs/unauth-icon.png';
          marker.setIcon(icon);
          marker.setVisible(true);

          google.maps.event.addListener(marker, 'mouseover', () => {
              this.infoWindow.setContent('<div class="infoWindowDescription"><p class="thankYou">Thank you!</p><p>Your notification has been sent to us. We will do our best to provide you with up to date statistics.</p></div>');
              this.infoWindow.open(map, marker);
          });

          this.infoWindow.setContent('<div class="infoWindowDescription"><p class="thankYou">Thank you!</p><p>Your notification has been sent to us. We will do our best to provide you with up to date statistics.</p></div>');
          this.infoWindow.open(map, marker);
          setTimeout(() => {
              this.infoWindow.close();
          }, 3000);

          this.unauthorizedMarkers.push(marker);
      }
      else if (type === 'car') {
        if (this.carIcon != null) {
          this.carIcon.setMap(null);
        }
        marker.setAnimation(null);
        icon.url = '../../assets/imgs/car.png';
        icon.scaledSize = new google.maps.Size(33, 33);
        marker.setIcon(icon);
        this.carIcon = marker;
        marker.setVisible(true);
        //this.currentLocationMarker.setMap(null);
        this.currentLocationMarker = marker;
        this.refreshMarkers();
        return;
      }

      google.maps.event.addListener(marker, 'click', () => {
        google.maps.event.addListener(this.infoWindow, 'domready', () => {
          if(info.voted === false) {
            let clickableUp = document.getElementById('infoWindowAccept' + info.id);
            clickableUp.addEventListener('click', () => {
              this.updateHotspotQueue(info.id, 'upvote');
              this.disableInfoWindowVote(info, 'default');
            });
            let clickableDown = document.getElementById('infoWindowRemove' + info.id);
            clickableDown.addEventListener('click', () => {
              console.log(info);
              this.updateHotspotQueue(info.id, 'downvote');
              this.disableInfoWindowVote(info, 'default');
            });
          }
        });
        this.infoWindow.open(map, marker);
        this.infoWindowHover = true;
      });
      google.maps.event.addListener(marker, 'mouseout', () => {
        if(!this.infoWindowHover) {
          this.infoWindow.close();
        }
        this.infoWindowHover = false;
      });
      return marker;
  };

  loadMarkers = (map) => {
    // if (this.dontShowMarkers) {
    //   this.firebaseProvider.switchDontShowMarkers();
    //   this.hotspots.forEach((hotspot) => {
    //     if(hotspot.visibleInTime === true){
    //       let marker = this.createMarker(hotspot, 'default', map);
    //       marker.setVisible(false);
    //     }
    //   });
    // }
    // else {
    //   console.log("YEP");
      this.hotspots.forEach((hotspot) => {
        if(hotspot.visibleInTime === true){
          this.createMarker(hotspot, 'default', map);
        }
      });
      this.refreshMarkers();
  };

  refreshMarkers = () => {
    if (this.Circle.visible === false) {
      this.markers.forEach((marker) => {
        marker.setVisible(false);
      });
      this.cdRef.detectChanges();
      return;
    }
    this.markers.forEach((marker) => {
      if((this.Circle.getBounds().contains(marker.getPosition()))&&(marker.probability >= this.probabilityRangeNumber)&&(marker.visibleInTime === true)) {
        marker.setVisible(true);
      }
      else {
        marker.setVisible(false);
      }
    });
    this.cdRef.detectChanges();
  };
// markers wont show

  deleteMarkers = () => {
    if(this.markers.length > 0)
      this.markers.forEach((marker) => {
        //marker.setVisible(false); IF IT DOESNT work
        marker.setMap(null);
      });
      this.markers.length = 0;
  };

  updateMarkers = () => {
    this.deleteMarkers();
    this.loadMarkers(this.globalMap);
    //this.refreshMarkers();
  }

  // getLocation = () => {
  //   this.geo.getCurrentPosition().then( pos => {
  //     let myLatLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
  //     this.globalMap.panTo(myLatLng);
  //     this.Circle.setCenter(myLatLng);
  //     this.Circle.setVisible(true);
  //     let info = {
  //       lat: myLatLng.lat(),
  //       long: myLatLng.lng()
  //     };
  //     this.createMarker(info, 'car', this.globalMap);
  //   })
  //   .catch (
  //     err => console.log(err)
  //   );
  // };

  startTracking = () => {
    if (this.trackingListener === true) {
      this.locationTracker.shouldResumeTracking = false;
      this.trackingListener = false;
      this.stopTracking();
      this.cdRef.detectChanges();
      return;
    }
    this.trackingListener = true;
    this.event.subscribe('updatePosition', (lat, lng) => {
      this.trackingLatLng = new google.maps.LatLng(lat, lng);
      this.globalMap.panTo(this.trackingLatLng);
      this.Circle.setCenter(this.trackingLatLng);
      this.Circle.setVisible(true);
      let info = {
        lat: this.trackingLatLng.lat(),
        long: this.trackingLatLng.lng()
      };
      this.createMarker(info, 'car', this.globalMap);
    });
    this.locationTracker.startTracking();
    this.locationTracker.shouldResumeTracking = true;
    this.cdRef.detectChanges();
  };

  stopTracking = () => {
    console.log("stopTracking");
    this.locationTracker.stopTracking();
    if(this.carIcon != null) {
      this.carIcon.setMap(null);
    }
    this.Circle.setVisible(false);
    this.event.unsubscribe('updatePosition');
    this.refreshMarkers();
  };

  showProbabilityRange = () => {
    this.probabilityRangeListener = true;
    this.isFabVisible = false;
    this.cdRef.detectChanges();
  };

  closeProbabilityRange = () => {
    this.probabilityRangeListener = false;
    this.isFabVisible = true;
    //this.refreshMarkers();
  };

}
