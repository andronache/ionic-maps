import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ActivityTracker } from '../../providers/activity-tracker/activity-tracker.service';
import { LocationTracker } from '../../providers/location-tracker/location-tracker.service';
import { AchievementsInterface } from '../../interfaces';

@Component({
  selector: 'achievements-page',
  templateUrl: 'achievements.html'
})
export class AchievementsPage {
  achievements: Array<AchievementsInterface>;

  constructor(public navCtrl: NavController, private activityTracker: ActivityTracker, private cdRef: ChangeDetectorRef, private locationTracker: LocationTracker) {
      this.activityTracker.cast.subscribe((achievements) => {
        this.achievements = achievements;
      });
  }

  public decrease = (load) => {
    this.activityTracker.decreaseScore(load);
  }

  itemTapped = (id) => {
    if (this.achievements[id].showDescription === false) {
      this.achievements[id].showDescription = true;
    }
    else {
      this.achievements[id].showDescription = false;
    }
    this.cdRef.detectChanges();
  }
}
