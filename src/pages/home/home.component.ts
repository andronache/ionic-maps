import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase.service';
import { CallNumber } from '@ionic-native/call-number';
import { AuthService } from '../../providers/auth/auth.service';

import { MapPage } from '../map/map.component';
import { AuthPage } from '../auth/auth.component';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // private phoneNumber: number = 0756600176;
  //authState: any;
  constructor(public nav: NavController, private callNumber: CallNumber, private auth: AuthService) {
    //this.modelRef = firebaseProvider.getItems();
    //this.modelRef.forEach(item => console.log(item));
    //console.log(this.modelRef);
    // auth.authStateRef.subscribe((authState) => {
    //   this.authState = authState;
    // });
    //this.name = this.authState.email.replace(/ @.*/, '');
  }

  call = () => {
    this.callNumber.callNumber("0756600176", true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }

  goToAuthPage = () => {
    this.nav.push(AuthPage);
  }

  goToMapPage = () => {
    this.nav.push(MapPage);
  }

}
