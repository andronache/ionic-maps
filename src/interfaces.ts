export interface AchievementsInterface {
  showDescription: boolean,
  name: string,
  description: string,
  id: number
}

export interface HotspotInterface {
  lat?: number;
  long?: number;
  id?: number;
  start?: number;
  end?: number;
  hour?: number;
  day?: string;
  probability?: number;
  appearances?: number;
  outOfTotal?: number;
  queue?: {
    upvote: number;
    downvote: number;
  };
  now?: boolean;
  flag?: boolean;
  voted?: boolean;
  update?: boolean;
  visibleInTime?: boolean;
}
