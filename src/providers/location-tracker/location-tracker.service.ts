import { Injectable, NgZone } from '@angular/core';
import { BackgroundGeolocation, BackgroundGeolocationConfig } from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/filter';

@Injectable()
export class LocationTracker {

  public watch: any;
  public lat: number = 0;
  public lng: number = 0;
  public shouldResumeTracking = false;
  public flag: boolean = false;

  constructor(public zone: NgZone, private backgroundGeolocation: BackgroundGeolocation, private geolocation: Geolocation, private event: Events) {

  }

  // const config: BackgroundGeolocationConfig = {};

  startTracking() {
    // background tracking
    // let config = {
    //   desiredAccuracy: 0,
    //   stationaryRadius: 20,
    //   distanceFilter: 10,
    //   debug: true,
    //   interval: 2000
    // };
    //
    // this.BackgroundGeolocation.configure(config).subscribe((location) => {
    //   console.log('BackgroundGeolocation:' + location.latitude + ' ' + location.longitude);
    //   this.zone.run(() => {
    //     this.lat = location.latitude;
    //     this.lng = location.longitude;
    //   });
    // }, (err) => {
    //   console.log(err);
    // });
    //
    // this.BackgroundGeolocation.start();

    let options = {
      frequency: 3000,
      enableHighAccuracy: true
    };

    this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {
      this.zone.run(() => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.event.publish('updatePosition', this.lat, this.lng);
      });
    });

    console.log("service subscribe");
  }

  stopTracking() {
    // this.BackgroundGeolocation.finish();
    this.watch.unsubscribe();
    console.log("service unsubscribe");
  }

}
