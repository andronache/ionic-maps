import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AchievementsInterface } from '../../interfaces';

@Injectable()
export class ActivityTracker {

  constructor() {

  }
  private scoreCheckpoints: Array<number> = [10, 20, 50, 100];
  private achievementsList: Array<AchievementsInterface> = [
    {
      id: 0,
      name: 'Getting Started',
      description: 'This is your first achievement. There are a lot more to come!',
      showDescription: false
    },
    {
      id: 1,
      name: 'Official Member',
      description: 'You\'re getting used to it. Keep up the good work!',
      showDescription: false
    },
    {
      id: 2,
      name: 'Professional Playa',
      description: 'The community needs you. You are one hell of a comrade!',
      showDescription: false
    },
    {
      id: 3,
      name: 'Admin SysOp & CEO',
      description: 'You are the King, the President and the CEO. Congratulations!',
      showDescription: false
    }
  ];
  private score = new BehaviorSubject<number>(0);
  private badges = new BehaviorSubject<Array<AchievementsInterface>>([]);
  cast = this.badges.asObservable();
  scoreRef = this.score.asObservable();

  scoreTimeout = () => {
    let interval = setInterval(() => {
      this.increaseScore(10);
    }, 1000);
    setTimeout(() => {
      clearInterval(interval);
    }, 10000);
  }

  checkScore = (load) => {
    let temp: number = 0;
    let terminated = false;
    this.scoreCheckpoints.forEach((checkpoint) => {
      if (checkpoint > load) {
        if(this.scoreCheckpoints.indexOf(checkpoint) > 0 && terminated === false) {
          temp = this.scoreCheckpoints[this.scoreCheckpoints.indexOf(checkpoint) - 1];
          terminated = true;
        }
      } // last achievement down here
      else {
        if (checkpoint <= load && this.scoreCheckpoints[this.scoreCheckpoints.indexOf(checkpoint)] === this.scoreCheckpoints[this.scoreCheckpoints.length - 1]) {
          temp = this.scoreCheckpoints[this.scoreCheckpoints.indexOf(checkpoint)];
        }
      }
    });
    return temp;
  };

  increaseScore = (load) => {
    let checkpoint = this.checkScore(this.score.getValue() + load);
    if (checkpoint > this.score.getValue()) {
      this.addAchievement(this.achievementsList[this.scoreCheckpoints.indexOf(checkpoint)]);
    }
    this.score.next(this.score.getValue() + load);
  };

  removeAchievement = (data) => { //unique ID
    let temp = this.badges.getValue();
    temp.forEach((badge) => {
      console.log(badge);
      console.log(data);
      if(badge.id === this.scoreCheckpoints.indexOf(data)) {
        temp.splice(temp.indexOf(badge), 1);
        console.log(temp);
        this.badges.next(temp);
      }
    });
  };

  decreaseScore = (load) => {
    let max = this.checkScore(this.score.getValue() - load);
    this.scoreCheckpoints.forEach((checkpoint) => {
      if (checkpoint > max) {
        this.removeAchievement(checkpoint);
      }
    });
    this.score.next(this.score.getValue() - load);
  };

  addAchievement = (data) => {
    let temp = this.badges.getValue();
    temp.push(data);
    this.badges.next(temp);
  };

  getScore = () => {
    return this.score;
  }


}
