import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {  FirebaseAuthState, AuthProviders, AuthMethods } from "angularfire2";
import { AngularFireDatabase } from "angularfire2/database"
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController } from 'ionic-angular';
import * as firebase from 'firebase';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  //user: Observable<firebase.User>;
  authState: any = null;
  //public authStateRef;

  constructor(public af: AngularFireAuth, public db: AngularFireDatabase, private alertCtrl: AlertController) {
    this.af.authState.subscribe((auth) => {
      this.authState = auth;
    });
    // authStateRef = this.authState.asObservable();

  }
  //
  // get authenticated(): boolean {
  //   return this.user !== null;
  // }
  //
  // get currentUser(): any {
  //   return this.authenticated ? this.user : null;
  // }
  //
  // get currentUserId(): string {
  //   return this.authenticated ? this.user : '';
  // }
  // get authState(): any {
  //   return this.authState;
  // }

  get authenticated(): boolean {
    return this.authState !== null;
  }

  // Returns current user data
  get currentUser(): any {
    return this.authenticated ? this.authState : null;
  }

  // Returns
  get currentUserObservable(): any {
    return this.af.authState
  }

  // Returns current user UID
  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  private socialSignIn(provider) {
    return this.af.auth.signInWithPopup(provider)
      .then((credential) =>  {
          this.user = credential.user;
          console.log(user);
          //this.updateUserData();
          return 'success';
      })
      .catch(error => {
        this.showError("Error on ", + provider + "sign in");
      });
  }

  // private updateUserData(): void {
  //   let path = `users/${this.currentUserId}`; // Endpoint on firebase
  //   let data = {
  //                name: this.user.displayName,
  //                email: this.user.email,
  //              }
  //   this.db.object(path).update(data)
  //   .catch(error => console.log(error));
  // }

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider()
    return this.socialSignIn(provider);
  }

  facebookLogin() {
    const provider = new firebase.auth.FacebookAuthProvider()
    return this.socialSignIn(provider);
  }

  logout(): void {
    this.af.auth.signOut();
    // MOTHERFUCKER: GO TO HOMEPAGE
  }

  login(email: string, password: string) {
    this.af
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
          return 'success';
      })
      .catch(err => {
        this.showError("Error on sign in");
      });
  }

  signup(email: string, password: string) {
    this.af
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(value => {
        return 'success';
      })
      .catch(err => {
        this.showError("Error on signup");
      });
  }

  resetPassword(email: string) {
    var auth = firebase.auth();

    return auth.sendPasswordResetEmail(email)
      .then(() => {
        return 'success';
      })
      .catch((error) => {
        this.showError("Error on reset password");
      })
  }

  showError(text) {
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
  // public login(credentials) {
  //   if (credentials.email === null || credentials.password === null) {
  //     return Observable.throw("Please insert credentials");
  //   } else {
  //     return Observable.create(observer => {
  //       // At this point make a request to your backend to make a real check!
  //       let access = (credentials.password === "pass" && credentials.email === "email");
  //       this.currentUser = new User('Simon', 'saimon@devdactic.com');
  //       observer.next(access);
  //       observer.complete();
  //     });
  //   }
  // }
  //
  // public register(credentials) {
  //   if (credentials.email === null || credentials.password === null) {
  //     return Observable.throw("Please insert credentials");
  //   } else {
  //     // At this point store the credentials to your backend!
  //     return Observable.create(observer => {
  //       observer.next(true);
  //       observer.complete();
  //     });
  //   }
  // }
  //
  // public getUserInfo() : User {
  //   return this.currentUser;
  // }
  //
  // public logout() {
  //   return Observable.create(observer => {
  //     this.currentUser = null;
  //     observer.next(true);
  //     observer.complete();
  //   });
  // }
}
