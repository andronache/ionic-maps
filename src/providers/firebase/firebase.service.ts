import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HotspotInterface } from "../../interfaces";
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as _ from 'lodash';
// import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
//
// import { DayInterface } from "../../interfaces";
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
// import 'rxjs/add/operator/map';

@Injectable()
export class FirebaseProvider {

  public canUpdateQueue: Array<boolean> = [];
  //private model: Array<DayInterface> = [];
  private model: AngularFirestoreCollection<Array<HotspotInterface>>;
  public modelRef: Observable<Array<HotspotInterface>>;
  public dontShowMarkers: boolean = true;
  //private model = new BehaviorSubject<Array<HotspotInterface>>([]);
  //modelRef = this.model.asObservable();

  constructor(private db: AngularFirestore) {
    this.loadDatabase();
    // this.canUpdateQueue = this.model.snapshotChanges().map(actions => {
    //   return actions.map(a => {
    //     console.log(a.payload.doc.id);
    //     return a.payload.doc.id;
    //   });
    // });
    //this.modelRef.map(item => console.log(item));
    // this.modelRef.map((item) => {
    //   console.log("dap", item);
    //   this.canUpdateQueue.push({id: item.id, permission: true});
    //   // console.log(item);
    // });
    // this.modelRef = this.model.snapshotChanges()
    //   .map(actions => {
    //     return actions.map(a => {
    //
    //       const data = a.payload.doc.data();
    //       const id = a.payload.doc.id;
    //       return { id, data };
    //     });
    //   });

    // this.model.next(this.loadDatabase()); //DB SYNC here
    // //this.startTrackingMinutes();
    // this.model.getValue().forEach(() => {
    //   this.canUpdateQueue.push({permission: true});
    // });

  }
  // 
  // switchDontShowMarkers = () => {
  //   if (this.dontShowMarkers === true) {
  //     this.dontShowMarkers = false;
  //     return;
  //   }
  //   this.dontShowMarkers = true;
  // }
  //
  // getDontShowMarkers = () => {
  //   return this.dontShowMarkers;
  // }

  startTrackingMinutes = () => {
    setInterval(() => {
      let time = this.getTimestamp();
      let ref = this.model.getValue();
      ref.forEach((hotspot) => {
        if (hotspot.visibleInTime === false && hotspot.start <= time && hotspot.end >= time) {
          hotspot.visibleInTime = true;
        }
        else if (hotspot.visibleInTime === true && (hotspot.start > time || hotspot.end < time)) {
          hotspot.visibleInTime = false;
        }
      });
    }, 1000);
  }

  updateItem = (item) => {
        let doc = this.db.doc<HotspotInterface>('hotspotsDb/' + item.id);
        item = this.checkIfNow(item);
        doc.update(item);
  }

  updateDatabase = () => {
    return;
  }

  loadDatabase = () => {
    this.model = this.db.collection('hotspotsDb');
    this.modelRef = this.model.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as HotspotInterface;
        const id = a.payload.doc.id;
        return { id, ...data };
      });
    });

  }

  checkIfNow = (hotspot) => {
    if (hotspot.now === false) {
      if (hotspot.queue.upvote > (2 * hotspot.queue.downvote)) {
        hotspot.now = true;
      }
    }
    else if (hotspot.queue.upvote < hotspot.queue.downvote) {
      hotspot.now = false;
    }
    return hotspot;
  }

  getTimestamp = () => {
    return 1;
  }

}
