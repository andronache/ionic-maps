import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HotspotInterface } from '../../interfaces';

// const UPLIMIT = 5;
// const RATIOLIMIT = 3;

@Injectable()
export class ProbabilityTracker {

  private modelRef = {
    Monday: {
      5: [
        {
          lat: 44.32,
          long: 23.8,
          id: 2,
          start: 4.55,
          end: 5.40,
          hour: 5,
          day: 'Monday',
          probability: 50,
          appearances: 3,
          outOfTotal: 6,
          queue: {
            upvote: 0,
            downvote: 0
          },
          now: false,
          flag: false
        }
      ]
    }
  };

  constructor() {
    //this.modelRef = firebase.getItems();
  }

  private notify = new BehaviorSubject<any>(0);
  cast = this.notify.asObservable();

  updateByHour = (d, h) => {
    this.modelRef[d][h].forEach((hotspot) => {
      if (hotspot.flag === true) {
              hotspot.appearances++;
            }
            hotspot.outOfTotal++;
            hotspot.probability = (hotspot.appearances / hotspot.outOfTotal) * 100;
            hotspot.queue = {upvote: 0, downvote: 0};
            hotspot.flag = false;
            console.log(hotspot.probability);
    });
    return this.modelRef[d][h];
    //this.firebase.applyChanges(d, h);
  };

  updateQueue = (data, load) => {
    this.modelRef[data.day][data.hour].forEach((hotspot) => {
      if(data.id === hotspot.id) {
        let info = {
          day: data.day,
          hour: data.hour,
          hotspot: this.modelRef[data.day][data.hour].indexOf(hotspot)
        };
        if(load.upvote > 0 && load.downvote === 0) {
          this.firebase.updateQueue(info, 'up');
        }
        else if (load.downvote > 0 && load.upvote === 0) {
          this.firebase.updateQueue(info, 'down');
        }

        console.log("up: ", hotspot.queue.upvote, "down: ", hotspot.queue.downvote);
        if ((hotspot.queue.upvote > UPLIMIT) && (hotspot.queue.upvote / hotspot.queue.downvote > RATIOLIMIT)) {
          hotspot.now = true;
          hotspot.flag = true;
          // NOTIFY SUBSCRIBERS
          this.notify.next(hotspot.id);
        }
        else {
          hotspot.now = false;
        }
      }
    });
  };

  getByHour = (d, h) => { return 1; }

  getByHotspot = () => {}

  getTime = () => {
    return 1;
  }
}

// updateByHour = (d, h) => {
//   let day = _.find(this.model, (day) => day.day === d);
//   let hour = _.find(day.hours, (hour) => hour.hour === h);
//   hour.hotspots.forEach((hotspot) => {
//     if (hotspot.flag === true) {
//       hotspot.appearances++;
//     }
//     hotspot.outOfTotal++;
//     hotspot.probability = (hotspot.appearances / hotspot.outOfTotal) * 100;
//     hotspot.queue = {upvote: 0, downvote: 0};
//   });
//   if (hour.hotspots !== undefined) {
//     return hour.hotspots;
//   }
//   else {
//     console.log("404 HOTSPOTS NOT FOUND  |  probability-tracker.service / updateByHour()");
//   }
//   // this.model.forEach((day) => {
//   //   if (day.day === d) {
//   //     day.hours.forEach((hour) => {
//   //       if (hour.hour === h) {
//   //         hour.hotspots.forEach((hotspot) => {
//   //           if (hotspot.flag === true) {
//   //             hotspot.appearances ++;
//   //           }
//   //           hotspot.outOfTotal ++;
//   //           hotspot.probability = (hotspot.appearances / hotspot.outOfTotal) * 100;
//   //           hotspot.queue = {upvote: 0, downvote: 0};
//   //         });
//   //         return hour; ???????????????????????????????????????????
//   //       }
//   //     });
//   //   }
//   // });
// };

// updateQueue = (data, load) => {
//   let day = _.find(this.model, (day) => day.day === data.day);
//   let hour = _.find(day.hours, (hour) => hour.hour === data.hour);
//   let hotspot = _.find(hour.hotspots, (spot) => spot.id === data.id);
//   if (hotspot !== undefined){
//     hotspot.queue.upvote += load.upvote;
//     hotspot.queue.downvote += load.downvote;
//     console.log("up: ", hotspot.queue.upvote, "down: ", hotspot.queue.downvote);
//     if ((hotspot.queue.upvote > UPLIMIT) && (hotspot.queue.upvote / hotspot.queue.downvote > RATIOLIMIT)) {
//       hotspot.now = true;
//       hotspot.flag = true;
//       // NOTIFY SUBSCRIBERS
//       this.notify.next(hotspot.id);
//       //EMIT???
//     }
//     else {
//       hotspot.now = false;
//     }
//   }
//   else {
//     console.log("404 HOTSPOT NOT FOUND  |  probability-tracker.service / updateQueue()");
//   }
//   // this.model.forEach((day) => {
//   //   if (day.day === data.day) {
//   //     day.hours.forEach((hour) => {
//   //       if (hour.hour === data.hour) {
//   //         hour.hotspots.forEach((hotspot) => {
//   //           if (hotspot.id === data.id) {
//   //             hotspot.queue.upvote += load.upvote;
//   //             hotspot.queue.downvote += load.downvote;
//   //             console.log("up: ", hotspot.queue.upvote, "down: ", hotspot.queue.downvote)
//   //             if ((hotspot.queue.upvote > UPLIMIT) && (hotspot.queue.upvote / hotspot.queue.downvote > RATIOLIMIT)) {
//   //               hotspot.now = true;
//   //               hotspot.flag = true;
//   //               // NOTIFY SUBSCRIBERS
//   //               this.notify.next(hotspot.id);
//   //               //EMIT???
//   //             }
//   //             else {
//   //               hotspot.now = false;
//   //             }
//   //           }
//   //         });
//   //       }
//   //     });
//   //   }
//   // });
// };

// getByHour = (d, h) => {
//   let day = _.find(this.model, (day) => day.day === d);
//   let hotspots = _.find(day.hours, (hour) => hour.hour === h).hotspots;
//   if (hotspots !== undefined) {
//     return hotspots;
//   }
//   else {
//     console.log("404 HOTSPOTS NOT FOUND  |  probability-tracker.service / getByHour()");
//   }
//   // let hotspotsArray: Array<HotspotInterface> = [];
//   // this.model.forEach((d) => {
//   //   if (d.day === day) {
//   //     day.hours.forEach((h) => {
//   //       if (h.hour === hour) {
//   //         hour.hotspots.forEach((hotspot) => {
//   //           hotspotsArray.push(hotspot);
//   //         });
//   //       }
//   //     });
//   //   }
//   // });
//   // return hotspotsArray;
// };
//
// getByHotspot = (data) => {
//   let day = _.find(this.model, (day) => day.day === data.day);
//   let hour = _.find(day.hours, (hour) => hour.hour === data.hour);
//   let hotspot = _.find(hour.hotspots, (spot) => spot.id === data.id);
//   if (hotspot !== undefined) {
//     return hotspot;
//   }
//   else {
//     console.log("404 HOTSPOT NOT FOUND  |  probability-tracker.service / getByHotspot()");
//   }
//   // this.model.forEach((day) => {
//   //   if (day.day === data.day) {
//   //     day.hours.forEach((hour) => {
//   //       if (hour.hour === data.hour) {
//   //         hour.hotspots.forEach((hotspot) => {
//   //           if (hotspot.id === data.id) {
//   //             temp = hotspot;
//   //           }
//   //         });
//   //       }
//   //     });
//   //   }
//   // });
// }

// database = {};
// const hotspot = [
//   {
//     id: 1,
//     appearances: 2,
//     totalDays: 3,
//     probability: 1,
//     lat:          45,
//     long:         25
//   },
//   {
//     id: 1,
//     appearances: 5,
//     totalDays: 34,
//     probability: 5,
//     lat:          46,
//     long:         26
//   },
//   {
//     id: 1,
//     appearances: 21,
//     totalDays: 34,
//     probability: 80,
//     lat:          47,
//     long:         25
//   }
// ]
//
// computeHotspot = (data) => {
//     let hotspotDay = data.day;
//
//     this.database.hotspotDay.forEach((day) => {
//         day.hotspot.forEach((hotspot) => {
//             if(this.overlaps(hotspot, data)) {
//                 this.increase(hotspot);
//                 //hotspot.increased = true;
//             }
//
//         });
//         // if hotspot was not increased today it means the probability must be decreased
//         day.hotspot.forEach((hotspot) => {
//             if(!hotspot.increased)
//                 this.decrease(hotspot);
//         });
//     });
// };
// recalcProbability = (hotspot) => {
//     hotspot.probability = hotspot.appearances / hotspot.totalDays;
//     console.log(hotspot);
// };
//
// increase = (hotspot) => {
//   hotspot.appearances ++;
//   hotspot.totalDays ++;
//   this.recalcProbability(hotspot);
// };
//
// decrease = (hotspot) => {
//   hotspot.totalDays ++;
//   this.recalcProbability(hotspot);
// };
//
// get = () => {
//   return hotspot;
// }
//
// test = () => {
//     hotspot.forEach((hotspot) => {
//         console.log(hotspot);
//         console.log("Increasing: ");
//         this.increase(hotspot);
//         console.log("Decreasing: ");
//         this.decrease(hotspot);
//     });
// }
