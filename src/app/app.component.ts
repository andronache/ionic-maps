import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home.component';
import { AuthPage } from '../pages/auth/auth.component';
import { AchievementsPage } from '../pages/achievements/achievements.component';
import { MapPage } from '../pages/map/map.component';
import { ActivityTracker } from '../providers/activity-tracker/activity-tracker.service';
import { AuthService } from '../providers/auth/auth.service';
//const google: any;

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  private score: number;
  private userLoggedIn: boolean = true;
  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public activityTracker: ActivityTracker, private auth: AuthService) {
    this.initializeApp();
    this.activityTracker.scoreRef.subscribe((score) => {
      this.score = score;
    });
    // this.auth.authState.subscribe((authState) => {
    //   this.authState = authState;
    // });
    // console.log(this.authState);
    // this.name = this.authState.email.replace(/ @.*/, '');
    // console.log(this.name);
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Map', component: MapPage },
      { title: 'Achievements', component: AchievementsPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  logout() {
    this.auth.logout();
  }

  login() {
    this.nav.setRoot(AuthPage);
  }

  openPage(page) {
    if (this.nav.getActive().component.name == 'MapPage' && page.title == 'Map') {
      return;
    }
    else
    this.nav.setRoot(page.component);
  }


}
