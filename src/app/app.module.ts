import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreModule } from 'angularfire2/firestore';

import { HttpModule } from '@angular/http';

import { IonicStorageModule, Storage } from '@ionic/storage';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 GoogleMapOptions,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';

import { CallNumber } from '@ionic-native/call-number';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home.component';
import { AchievementsPage } from '../pages/achievements/achievements.component';
import { MapPage } from '../pages/map/map.component';
import { AuthPage } from '../pages/auth/auth.component';


import { ProbabilityTracker } from '../providers/probability-tracker/probability-tracker.service';
import { FirebaseProvider } from '../providers/firebase/firebase.service';
import { LocationTracker } from '../providers/location-tracker/location-tracker.service';
import { ActivityTracker } from '../providers/activity-tracker/activity-tracker.service';
import { AuthService } from '../providers/auth/auth.service';

const firebaseConfig = {
  apiKey: "AIzaSyClHJ7dios4sKo1DiJpbqRUIYX9_w1MHS8",
  authDomain: "ionicmaps-ccbe8.firebaseapp.com",
  databaseURL: "https://ionicmaps-ccbe8.firebaseio.com",
  projectId: "ionicmaps-ccbe8",
  storageBucket: "",
  messagingSenderId: "395299017733"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AchievementsPage,
    MapPage,
    AuthPage
  ],
  imports: [
    BrowserModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AchievementsPage,
    MapPage,
    AuthPage
  ],
  providers: [
    LocationTracker,
    BackgroundGeolocation,
    StatusBar,
    SplashScreen,
    Geolocation,
    GoogleMaps,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseProvider,
    ProbabilityTracker,
    ActivityTracker,
    CallNumber,
    AuthService
  ]
})
export class AppModule {}
